# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2018-07-18 03:29+0200\n"
"PO-Revision-Date: 2019-01-24 19:39+0100\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 19.03.70\n"

#: mcl_settings.py:43
msgid "MiddleClickLoader Setting"
msgstr "Ustawienia MiddleClickLoader"

#: mcl_settings.py:44
msgid "MiddleClickLoader"
msgstr "MiddleClickLoader"

#: mcl_settings.py:45
msgid "Open url in:"
msgstr "Otwórz adres url w:"

#: mcl_settings.py:46
msgid "Use only valid url"
msgstr "Używaj tylko prawidłowych adresów url"

#: mcl_settings.py:48
msgid "New Tab"
msgstr "Nowa karta"

#: mcl_settings.py:49
msgid "Current Tab"
msgstr "Bieżąca karta"

#: mcl_settings.py:50
msgid "New Window"
msgstr "Nowe okno"